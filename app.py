#!/usr/bin/env python3

import aws_cdk as cdk

from cp_dep_1.cp_dep_1_stack import CpDep1Stack


app = cdk.App()
CpDep1Stack(app, "CpDep1Stack")

app.synth()
