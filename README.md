# api-postales-fastapi

Api de los códigos postales de México utilizando fastapi, dynamodb y aws lambda.

## Arquitectura

La aplicación actual está desplegada en AWS utilizando lambdas de AWS, con el framework de python fastapi utilizando un wrapper asgi a aws lambda llamado mangum y como base de datos tablas de dynamodb.

A continuación se puede ver la arquitectura de este proyecto:

![Arquitectura](https://gitlab.com/electrototo/api-postales-fastapi/-/raw/681fe37841420890daf6a1a9f6e9726329d4c4f3/docs/images/architecture.png "Diagrama arquitectura")

## Descripción de la aplicación

En la aplicación están registrados dos endpoints principales: `/zip-codes/` y `/states/`. Se utilizó `fastapi` para la creación del API. Está desarrollada utilizando el cdk de AWS, por lo que toda la infraestructura necesaria para desplegar la aplicación está definida dentro de los archivos `cp_dep_1/cp_dep_1_stack.py` y `cp_dep_1/api_service.py`. El código de la API,la cual la lambda se va a encargar de ejecutar se encuentra en la carpeta de `api/`.

Debido a que se utilizó una base de datos NoSQL, el diseño de la base de datos es distinta a la de una base de datos SQL.
* En la tabla `zip_codes` el _partition key_ es el código postal (d_codigo), mientras que el _sort key_ es el identificador único del asentamiento a nivel municipal (id_asenta_cpcons).
* En la tabla `states` el _partition key_ es un número entero generado por el usuario al crear un nuevo registro.

### states

Dentro de `/states/` se pueden hacer todas las operaciones CRUD para obtener los estados almacenados en la base de datos.

### zip-codes

Dentro de zip codes se pueden listar los códigos postales, obtener un asentamiento en específico y crear un registro de código postal.

## Documentación de los endpoints

La documentanción de los endpoints fue autogenerada por `fastapi` utilizando el formato `openapi` y se puede consultar accediendo al endpoint `/redoc`.

## Despliegue de la aplicación

Se tienen que seguir los siguientes pasos para desplegar de forma exitosa esta aplicación:
1. Tener configurado previamente `cdk` de aws en la máquina de desarrollo.
2. Ejecutar `cdk deploy`.
