import aws_cdk as core
import aws_cdk.assertions as assertions

from cp_dep_1.cp_dep_1_stack import CpDep1Stack

# example tests. To run these tests, uncomment this file along with the example
# resource in cp_dep_1/cp_dep_1_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = CpDep1Stack(app, "cp-dep-1")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
