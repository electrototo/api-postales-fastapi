from constructs import Construct
from aws_cdk import (
    aws_apigateway as apigateway,
    aws_dynamodb as dynamodb,
    aws_lambda as lambda_,
)

import subprocess


class ApiService(Construct):
    def __init__(self, scope: Construct, id: str):
        super().__init__(scope, id)

        zip_codes_table = dynamodb.Table(
            self,
            'ZipCodesTable',
            table_name='zip_codes',
            partition_key=dynamodb.Attribute(
                name='code',
                type=dynamodb.AttributeType.STRING
            ),
            sort_key=dynamodb.Attribute(
                name='asenta_id',
                type=dynamodb.AttributeType.NUMBER
            ),
            billing_mode=dynamodb.BillingMode.PROVISIONED,
        )

        states_table = dynamodb.Table(
            self,
            'StatesTable',
            table_name='states',
            partition_key=dynamodb.Attribute(
                name='id',
                type=dynamodb.AttributeType.NUMBER
            ),
            billing_mode=dynamodb.BillingMode.PROVISIONED,
        )

        handler = lambda_.Function(
            self,
            'ApiHandler',
            runtime=lambda_.Runtime.PYTHON_3_8,
            code=lambda_.Code.from_asset('api'),
            handler='api.main.handler',
            layers=[
                self.create_dependencies_layer('api')
            ]
        )

        api = apigateway.RestApi(
            self,
            'api',
            rest_api_name='Zip code and states service',
            description='This service serves zip codes and states'
        )

        integration = apigateway.LambdaIntegration(
            handler
        )

        api.root.add_proxy(any_method=True, default_integration=integration)

        zip_codes_table.grant_full_access(handler)
        states_table.grant_full_access(handler)

    def create_dependencies_layer(self, function_name: str):
        requirements = 'api/requirements.txt'
        output_dir = f'.build/{function_name}'

        subprocess.check_call(
            f'pip install -r {requirements} -t {output_dir}/python'.split()
        )

        layer_code = lambda_.Code.from_asset(output_dir)

        return lambda_.LayerVersion(self, 'api-dependencies', code=layer_code)
