from fastapi import FastAPI

from api.routers import states, zip_codes
from mangum import Mangum


app = FastAPI(root_path='/prod')
app.include_router(states.router)
app.include_router(zip_codes.router)

handler = Mangum(app)
