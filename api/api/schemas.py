from pydantic import BaseModel
from typing import List, Optional


class Message(BaseModel):
    message: str


class State(BaseModel):
    id: int
    name: str


class PostalServiceIn(BaseModel):
    code: str
    settlement: str
    settlement_type: str
    municipality: str
    city: Optional[str] = None
    zone: str
    state: int
    asenta_id: int


class PostalAnswer(BaseModel):
    status: bool = True
    payload: List[PostalServiceIn]


class StatesAnswer(BaseModel):
    status: bool = True
    payload: List[State]
