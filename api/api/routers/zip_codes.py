from fastapi import APIRouter
from fastapi.responses import JSONResponse

from api.schemas import Message, PostalAnswer, PostalServiceIn

import boto3
from boto3.dynamodb.conditions import Key


router = APIRouter(
    prefix='/zip-codes',
    tags=['zip-codes'],
)

ddb = boto3.resource('dynamodb')


@router.get('/{zip_code}', response_model=PostalAnswer)
async def list_zip_codes(zip_code: str):
    table = ddb.Table('zip_codes')

    items = table.query(
        KeyConditionExpression=Key('code').eq(zip_code)
    )

    return {'payload': items.get('Items', [])}


@router.get(
    '/{zip_code}/{asenta_id}',
    response_model=PostalServiceIn,
    responses={404: {'model': Message}}
)
async def get_zip_code(zip_code: str, asenta_id: int):
    table = ddb.Table('zip_codes')

    ddb_response = table.get_item(
        Key={
            'code': zip_code,
            'asenta_id': asenta_id
        }
    )

    if 'Item' not in ddb_response:
        return JSONResponse(status_code=404, content={'message': 'Item not found'})

    return ddb_response['Item']


@router.post('/', response_model=PostalServiceIn, status_code=201)
async def create_zip_code(item: PostalServiceIn):
    table = ddb.Table('zip_codes')

    table.put_item(
        Item=dict(item)
    )

    return item
