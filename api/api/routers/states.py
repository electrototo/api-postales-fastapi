from fastapi import APIRouter, Response
from fastapi.responses import JSONResponse

from api.schemas import Message, State, StatesAnswer

import boto3


router = APIRouter(
    prefix='/states',
    tags=['states'],
)

ddb = boto3.resource('dynamodb')


@router.get('/', response_model=StatesAnswer)
async def list_states():
    table = ddb.Table('states')

    ddb_response = table.scan()

    return {'payload': ddb_response['Items']}


@router.post('/', response_model=State, status_code=201)
async def create_state(state: State):
    table = ddb.Table('states')

    table.put_item(
        Item=dict(state)
    )

    return state


@router.get('/{state_id}', response_model=State, responses={404: {'model': Message}})
async def get_state(state_id: int, response: Response):
    table = ddb.Table('states')

    ddb_response = table.get_item(
        Key={
            'id': state_id
        }
    )

    if 'Item' not in ddb_response:
        return JSONResponse(status_code=404, content={'message': 'Item not found'})

    return ddb_response['Item']


@router.delete('/{state_id}', response_model=Message, responses={404: {'model': Message}})
async def delete_state(state_id: int, response: Response):
    table = ddb.Table('states')

    table.delete_item(
        Key={
            'id': state_id
        }
    )

    return {'message': 'ok'}
